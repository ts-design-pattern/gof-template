/**
 * 專案名稱： gof-adapter
 * 檔案說明： Kafka 資料消費者轉接器
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';
import { ConsumerAdapter } from '../../core';
import { ConsumerGroup } from '../kafka';

/**
 * Kafka 資料消費者轉接器
 */
export class KafkaConsumerAdapter<D = any> extends ConsumerAdapter<
  ConsumerGroup,
  D
> {
  /**
   * @param consumer 資料消費者
   */
  constructor(protected consumer: ConsumerGroup) {
    super(consumer);
  }

  /**
   * 消費資料
   *
   * @method public
   * @return 取得要消費的資料
   */
  public consume(): Observable<D> {
    return new Observable(sub => {
      this.consumer.on('message', msg =>
        sub.next(JSON.parse(msg.value.toString())),
      );
    });
  }
}
