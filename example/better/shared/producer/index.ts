/**
 * 專案名稱： gof-template
 * 檔案說明： 資料生產者匯出點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

export * from './email-producer.adapter';
export * from './kafka-producer.adapter';
