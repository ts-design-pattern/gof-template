/**
 * 專案名稱： gof-template
 * 檔案說明： Email 資料生產者轉接器
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { ProducerAdapter, PublishCallback } from '../../core';
import { Email, Mail } from '../email';

/**
 * Email 資料生產者轉接器
 */
export class EmailProducerAdapter extends ProducerAdapter<Email, Mail> {
  /**
   * @param producer 資料生產者
   */
  constructor(protected producer: Email) {
    super(producer);
  }

  /**
   * 上拋資料
   *
   * @method public
   * @param data     資料
   * @param callback 上拋回呼
   */
  public publish(data: Mail, callback?: PublishCallback): void {
    this.producer.send(data, (error, result) => {
      if (callback) {
        callback(error, result);
      }
    });
  }
}
