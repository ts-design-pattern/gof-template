/**
 * 專案名稱： gof-template
 * 檔案說明： Kafka 資料生產者轉接器
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { ProducerAdapter, PublishCallback } from '../../core';
import { HighLevelProducer, Payload } from '../kafka';

/**
 * Kafka 資料生產者轉接器
 */
export class KafkaProducerAdapter extends ProducerAdapter<
  HighLevelProducer,
  Payload
> {
  /**
   * @param producer 資料生產者
   */
  constructor(protected producer: HighLevelProducer) {
    super(producer);
  }

  /**
   * 上拋資料
   *
   * @method public
   * @param data     資料
   * @param callback 上拋回呼
   */
  public publish(data: Payload, callback?: PublishCallback): void {
    this.producer.send(data, (error, result) => {
      if (callback) {
        callback(error, result);
      }
    });
  }
}
