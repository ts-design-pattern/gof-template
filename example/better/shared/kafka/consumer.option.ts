/**
 * 專案名稱： gof-adapter
 * 檔案說明： Consumer 配置
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * Consumer 配置
 */
export const CONSUMERT_OPTS = {
  kafkaHost: 'broker:9092',
  batch: undefined,
  ssl: true,
  groupId: 'ExampleTestGroup',
  sessionTimeout: 15000,
  protocol: ['roundrobin'],
  encoding: 'utf8',
  fromOffset: 'latest',
  commitOffsetsOnFirstJoin: true,
  outOfRangeOffset: 'earliest',
};
