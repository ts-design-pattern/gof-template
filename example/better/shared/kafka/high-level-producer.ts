/**
 * 專案名稱： gof-template
 * 檔案說明： Kafka Producer
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 上拋資料格式
 */
export type Payload<D = any> = { topic: string; message: D };

/**
 * Kafka Producer
 */
export class HighLevelProducer<T = any> {
  /**
   * 上拋資料
   *
   * @method public
   * @param payload   資料包裹
   * @param callback 上拋後的回乎函數
   */
  public send(
    payload: Payload<T>,
    callback?: (error: Error | null, data: T) => void,
  ): void {
    if (callback) {
      callback(null, payload.message);
    }
  }
}
