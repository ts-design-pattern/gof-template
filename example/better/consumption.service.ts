/**
 * 專案名稱： gof-template
 * 檔案說明： 用電量計算服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Consumer, Producer, ServiceTemplate } from './core';
import {
  ConsumerGroup,
  Consumption,
  HighLevelProducer,
  KafkaConsumerAdapter,
  KafkaProducerAdapter,
  Payload,
  Reading,
} from './shared';

/**
 * 用電量計算服務
 */
export class ConsumptionService extends ServiceTemplate<
  Reading,
  Consumption,
  Payload
> {
  /**
   * 電表讀數
   */
  private _reading = new Map<string, Reading>();

  /**
   * 取得資料消費者
   *
   * @method public
   * @return 回傳資料消費者
   */
  public async consumer(): Promise<Consumer> {
    const kafkaHost = 'localhost:1883';
    const groupId = 'test.group.id';
    const source = 'wks.cim.m1.meter.reading';
    const consumerGroup = new ConsumerGroup({ kafkaHost, groupId }, source);
    return new KafkaConsumerAdapter(consumerGroup);
  }

  /**
   * 資料處理
   *
   * @method public
   * @param message 消費的資料
   * @return 回傳處理後的資料
   */
  public async process(message: Reading): Promise<Consumption | null> {
    const reading = message;
    const existReading = this._reading.get(reading.meterId);
    this._reading.set(reading.meterId, reading);
    if (existReading) {
      // 當有前一筆讀數，計算用電量，並發送
      return {
        evt_dt: new Date().getTime(),
        building: reading.building,
        meterId: reading.meterId,
        consumption: reading.reading - existReading.reading,
      };
    } else {
      return null;
    }
  }

  /**
   * 打包資料
   *
   * @method public
   * @param data 處理後的資料
   * @return 回傳打包後的資料
   */
  public async payload(data: Consumption): Promise<Payload> {
    const topic = 'wks.cim.t1.meter.consumption';
    return { topic, message: data };
  }

  /**
   * 取得資料生產者
   *
   * @method public
   * @return 回傳資料生產者
   */
  public async producer(): Promise<Producer> {
    const producer = new HighLevelProducer<Consumption>();
    return new KafkaProducerAdapter(producer);
  }
}
