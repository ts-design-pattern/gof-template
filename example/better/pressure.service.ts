/**
 * 專案名稱： gof-template
 * 檔案說明： 空壓報警服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Consumer, Producer, ServiceTemplate } from './core';
import {
  Email,
  EmailProducerAdapter,
  Mail,
  Mqtt,
  MqttConsumerAdapter,
  Pressure,
} from './shared';

/**
 * 空壓報警服務
 */
export class PressureService extends ServiceTemplate<Pressure, Pressure, Mail> {
  /**
   * 取得資料消費者
   *
   * @method public
   * @return 回傳資料消費者
   */
  public async consumer(): Promise<Consumer> {
    const client = new Mqtt().connect('mqtt://localhost:1883/');
    const topic = 'wks/cim/m1/pressure/#';
    return new MqttConsumerAdapter(client, topic);
  }

  /**
   * 資料處理
   *
   * @method public
   * @param message 消費的資料
   * @return 回傳處理後的資料
   */
  public async process(message: Pressure): Promise<Pressure | null> {
    if (message.pressure > 800) {
      return message;
    } else {
      return null;
    }
  }

  /**
   * 打包資料
   *
   * @method public
   * @param data 處理後的資料
   * @return 回傳打包後的資料
   */
  public async payload(data: Pressure): Promise<Mail> {
    return {
      from: 'alarm@wistron.com',
      to: 'user@wistron.com',
      subject: 'Pressure Alerm',
      context: `${data.machine} pressure over: ${data.pressure}`,
    };
  }

  /**
   * 取得資料生產者
   *
   * @method public
   * @return 回傳資料生產者
   */
  public async producer(): Promise<Producer> {
    const email = new Email('smtp.email', 587);
    return new EmailProducerAdapter(email);
  }
}
