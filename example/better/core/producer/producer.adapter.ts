/**
 * 專案名稱： wistroni40-dmc
 * 檔案說明： 資料生產者轉接器
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Producer, PublishCallback } from './producer';

/**
 * 資料生產者轉接器
 */
export abstract class ProducerAdapter<P = any, D = any> implements Producer<D> {
  /**
   * @param producer 資料生產者
   */
  constructor(protected producer: P) {}

  /**
   * 上拋資料
   *
   * @method public
   * @param data     資料
   * @param callback 上拋回呼
   */
  public abstract publish(data: D[] | D, callback?: PublishCallback): void;
}
