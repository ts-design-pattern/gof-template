/**
 * 專案名稱： gof-template
 * 檔案說明： 抽象服務範本
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { Producer } from '..';
import { Consumer } from '../consumer';

/**
 * 抽象服務範本
 *
 * @template M 消費的資料型別
 * @template D 處理後的資料型別
 * @template P 打包後的資料型別
 */
export abstract class ServiceTemplate<M = any, D = any, P = any> {
  /**
   * 取得資料消費者
   *
   * @method public
   * @return 回傳資料消費者
   */
  public abstract consumer(): Promise<Consumer>;

  /**
   * 資料處理
   *
   * @method public
   * @param message 消費的資料
   * @return 回傳處理後的資料
   */
  public abstract process(message: M): Promise<D | null>;

  /**
   * 打包資料
   *
   * @method public
   * @param data 處理後的資料
   * @return 回傳打包後的資料
   */
  public abstract payload(data: D): Promise<P>;

  /**
   * 取得資料生產者
   *
   * @method public
   * @return 回傳資料生產者
   */
  public abstract producer(): Promise<Producer>;

  /**
   * 發送結果日誌
   *
   * @method public
   * @param error  錯誤訊息
   * @param result 上拋結果
   */
  public print(error: Error | null, result: any): void {
    if (error) {
      console.error('Send failed');
      console.error(error);
    } else {
      console.log('Send successfully');
      console.log(JSON.stringify(result));
    }
  }

  /**
   * 執行服務
   *
   * @method public
   * @return 回傳執行結果
   */
  public async execute(): Promise<void> {
    const consumer = await this.consumer();
    const producer = await this.producer();
    const subject = consumer.consume().pipe(
      // 將消費的資料進行處理
      mergeMap(message => this.process(message)),
      // 過濾 NULL 的數據
      filter(data => data !== null),
      // 強制轉型為 NULL 數據
      map(data => data as D),
      // 將處理後的資料進行打包
      mergeMap(data => this.payload(data)),
      // 上拋資料
      tap(payload => producer.publish(payload, this.print.bind(this))),
    );
    subject.subscribe();
  }
}
