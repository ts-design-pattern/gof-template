/**
 * 專案名稱： gof-adapter
 * 檔案說明： 資料消費者匯出點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

export * from './consumer';
export * from './consumer.adapter';
