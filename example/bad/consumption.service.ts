/**
 * 專案名稱： gof-template
 * 檔案說明： 用電量計算服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import {
  ConsumerGroup,
  Consumption,
  HighLevelProducer,
  Reading,
} from './shared';

/**
 * 用電量計算服務
 */
export class ConsumptionService {
  /**
   * 電表讀數
   */
  private _reading = new Map<string, Reading>();

  /**
   * 執行服務
   *
   * @method public
   */
  public execute(): void {
    // 初始化 Kafka Consumer 及 Producer
    const kafkaHost = 'localhost:1883';
    const groupId = 'test.group.id';
    const source = 'wks.cim.m1.meter.reading';
    const target = 'wks.cim.t1.meter.consumption';
    const consumerGroup = new ConsumerGroup({ kafkaHost, groupId }, source);
    const producer = new HighLevelProducer<Consumption>();

    // 計算用電量
    consumerGroup.on('message', message => {
      const reading: Reading = JSON.parse(message.value.toString());
      const existReading = this._reading.get(reading.meterId);
      if (existReading) {
        // 當有前一筆讀數，計算用電量，並發送
        const consumption: Consumption = {
          evt_dt: new Date().getTime(),
          building: reading.building,
          meterId: reading.meterId,
          consumption: reading.reading - existReading.reading,
        };
        producer.send({ topic: target, message: consumption }, (err, res) => {
          if (err) {
            console.error('Send failed');
            console.error(err);
          } else {
            console.log('Send successfully');
            console.log(JSON.stringify(res));
          }
        });
      }
      this._reading.set(reading.meterId, reading);
    });
  }
}
