/**
 * 專案名稱： gof-template
 * 檔案說明： 不好的範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { ConsumptionService } from './consumption.service';
import { PressureService } from './pressure.service';

// 計算用電
setTimeout(() => {
  console.log('Process meter consumption');
  const consumptionService = new ConsumptionService();
  consumptionService.execute();
}, 500);

// 空壓報警
setTimeout(() => {
  console.log('Process pressure alarm');
  const pressureService = new PressureService();
  pressureService.execute();
}, 6500);
