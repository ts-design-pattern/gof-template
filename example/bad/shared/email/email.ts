/**
 * 專案名稱： gof-template
 * 檔案說明： 電子郵件發送功能
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 郵件發送內容
 */
export type Mail = {
  from: string;
  to: string;
  subject: string;
  context: string;
};

/**
 * 電子郵件發送功能
 */
export class Email {
  /**
   * @param _host SMTP Host
   * @param _port SMTP Port
   */
  constructor(private _host: string, private _port: number) {}

  /**
   * 發送郵件
   *
   * @method public
   * @param content  郵件內容
   * @param callback 發送後的回乎函數
   */
  public send(
    content: Mail,
    callback?: (error: Error | null, information: string) => void,
  ): void {
    if (callback) {
      callback(null, `Send mail to ${content.to} successfully`);
    }
  }
}
