/**
 * 專案名稱： gof-template
 * 檔案說明： 空壓資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */
/* eslint-disable camelcase */

/**
 * 空壓資料模型
 */
export interface Pressure {
  /**
   * 時間戳
   */
  evt_dt: number;
  /**
   * 建築
   */
  building: string;
  /**
   * 設備名稱
   */
  machine: string;
  /**
   * 空壓壓力
   */
  pressure: number;
}
