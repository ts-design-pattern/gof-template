/**
 * 專案名稱： gof-template
 * 檔案說明： 電表讀數資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */
/* eslint-disable camelcase */

/**
 * 電表讀數資料模型
 */
export interface Reading {
  /**
   * 時間戳
   */
  evt_dt: number;
  /**
   * 電表所屬建築
   */
  building: string;
  /**
   * 電表 ID
   */
  meterId: string;
  /**
   * 電表讀數
   */
  reading: number;
}
