/**
 * 專案名稱： gof-template
 * 檔案說明： 空壓報警服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Email, Mail, Mqtt, Pressure } from './shared';

/**
 * 空壓報警服務
 */
export class PressureService {
  /**
   * 執行服務
   *
   * @method public
   */
  public execute(): void {
    // 初始化 Mqtt
    const mqttClient = new Mqtt().connect('mqtt://localhost:1883/');
    const source = 'wks/cim/m1/pressure/#';
    mqttClient.on('connect', () => mqttClient.subscribe(source));

    // 初始化郵件
    const email = new Email('smtp.email', 587);

    // 判斷異常壓力
    mqttClient.on('message', (topic, message) => {
      const pressure: Pressure = JSON.parse(message);
      if (pressure.pressure > 800) {
        const content: Mail = {
          from: 'alarm@wistron.com',
          to: 'user@wistron.com',
          subject: 'Pressure Alerm',
          context: `${pressure.machine} pressure over: ${pressure.pressure}`,
        };
        email.send(content, (err, res) => {
          if (err) {
            console.error('Send failed');
            console.error(err);
          } else {
            console.log('Send successfully');
            console.log(content.context);
          }
        });
      }
    });
  }
}
