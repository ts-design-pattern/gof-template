/**
 * 專案名稱： gof-template
 * 檔案說明： 範本模式範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import * as RECOMMANDATION from './../mock/data/recommandation.json';
import * as STAR_PLAN from './../mock/data/start-plan.json';
import { RecommandationService, StarPlanService } from './shared';

// 大學推甄
new RecommandationService(RECOMMANDATION).execute();

// 繁星計畫
new StarPlanService(STAR_PLAN).execute();
