/**
 * 專案名稱： gof-template
 * 檔案說明： 大學推甄服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import {
  ApplicationPacket,
  Information,
  Package,
  UniversityTemplate,
} from './../core';

/**
 * 大學推甄服務
 */
export class RecommandationService extends UniversityTemplate {
  /**
   * @param pckg 申請包裹
   */
  constructor(protected pckg: Package) {
    super(pckg);
  }

  /**
   * 提供學生基本資料
   *
   * @method public
   * @return 回傳學生基本資料
   */
  public get information(): Information {
    return this.pckg.information;
  }

  /**
   * 提供備審資料
   *
   * @method public
   * @return 回傳備審資料
   */
  public get applicationPacket(): ApplicationPacket {
    if (this.pckg.recommandation) {
      return this.pckg.recommandation;
    } else {
      this.notificate('missing recommandation applicatoin packet');
      throw new Error('missing recommandation applicatoin packet');
    }
  }

  /**
   * 審核
   *
   * @method public
   * @return 回傳審核結果
   */
  public review(packet: ApplicationPacket): boolean {
    return packet.percentileRank > 75;
  }
}
