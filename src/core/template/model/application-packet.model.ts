/**
 * 專案名稱： gof-template
 * 檔案說明： 備審資料資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 備審資料資料模型
 */
export interface ApplicationPacket {
  /**
   * 備審資料類型
   */
  type: 'recommandation' | 'star-plan' | string;
  /**
   * PR 值
   */
  percentileRank: number;
}
