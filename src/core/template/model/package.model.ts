/**
 * 專案名稱： gof-template
 * 檔案說明： 申請包裹資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { ApplicationPacket } from 'src/core';
import { Information } from './information.model';

/**
 * 申請包裹資料模型
 */
export interface Package {
  /**
   * 學生基本資料
   */
  information: Information;
  /**
   * 推甄備審資料
   */
  recommandation?: ApplicationPacket;
  /**
   * 繁星計畫備審資料
   */
  starPlan?: ApplicationPacket;
}
