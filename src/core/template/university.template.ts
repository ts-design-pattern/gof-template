/**
 * 專案名稱： gof-template
 * 檔案說明： 抽象升學範本
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { ApplicationPacket, Information, Package } from './model';

/**
 * 審查結果資料模型
 */
export type ReviewResult = {
  info: Information;
  packet: ApplicationPacket;
  admission: boolean;
};

/**
 * 抽象升學範本
 */
export abstract class UniversityTemplate {
  /**
   * @param pckg 申請包裹
   */
  constructor(protected pckg: Package) {}

  /**
   * 提供學生基本資料
   *
   * @method public
   * @return 回傳學生基本資料
   */
  public abstract get information(): Information;

  /**
   * 提供備審資料
   *
   * @method public
   * @return 回傳備審資料
   */
  public abstract get applicationPacket(): ApplicationPacket;

  /**
   * 審核
   *
   * @method public
   * @return 回傳審核結果
   */
  public abstract review(packet: ApplicationPacket): boolean;

  /**
   * 通知結果
   *
   * @method public
   * @param result 審查結果
   */
  public notificate(result: string): void {
    console.log(result);
  }

  /**
   * 執行審查
   *
   * @method public
   */
  public execute(): void {
    const info = this.information;
    const packet = this.applicationPacket;
    const admission = this.review(packet);
    const result = { info, packet, admission };
    this.notificate(
      `${result.info.name} ${packet.type} admission result ${
        result.admission ? 'YES' : 'NO'
      }`,
    );
  }
}
