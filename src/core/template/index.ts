/**
 * 專案名稱： gof-template
 * 檔案說明： 範本匯出點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

export * from './model';
export * from './university.template';
